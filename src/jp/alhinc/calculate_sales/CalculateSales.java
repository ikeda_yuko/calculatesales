package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 支店定義ファイル名正規表現
	private static final String REGEX_BRANCH_LST = "^[0-9]{3}$";

	// 商品定義ファイル名正規表現
	private static final String REGEX_COMMODITY_LST = "^[A-Za-z0-9]{8}$";

	// 売上ファイル名正規表現
	private static final String REGEX_RCD_FILE_NAME = "^[0-9]{8}\\.rcd$";

	// 売上金額正規表現（数字）
	private static final String REGEX_NUMBER = "^[0-9]+$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String NO_CONTINUE_FILE_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILENAME_FORMAT_ERROR = "のフォーマットが不正です";
	private static final String BRANCH_CODE_NOT_EXIST = "の支店コードが不正です";
	private static final String COMMODITY_CODE_NOT_EXIST = "の商品コードが不正です";
	private static final String OVER_TEN_DIGITS = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		// 3(3-1)コマンドライン引数が1つ設定されているかチェック（×エラー表示、処理終了）
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, "支店", REGEX_BRANCH_LST)) {
			return;
		}

		// 商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, "商品", REGEX_COMMODITY_LST)) {
			return;
		}

		// ①ディレクトリから全フォルダを取得して配列filesに格納
		File[] files = new File(args[0]).listFiles();

		// ②売上ファイルの当てはまるものを格納するリストrcdFilesを宣言
		List<File> rcdFiles = new ArrayList<>();

		// ③filesの全要素について、売上ファイルの条件を満たすか判定（繰り返し）
		for(int i = 0; i < files.length; i++) {
			String filename = files[i].getName();

			// 3(3-3)売上ファイルがディレクトリでなくファイルであり、ファイル名が数字8桁.rcdか
			if((files[i].isFile()) && (filename.matches(REGEX_RCD_FILE_NAME))) {
				rcdFiles.add(files[i]);
			}
		}

		// 3(2-1)rcdFilesをソートし、売上ファイルが連番かチェック（×エラー表示、処理終了）
		Collections.sort(rcdFiles);
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(NO_CONTINUE_FILE_NUMBER);
				return;
			}
		}

        // ④条件を満たしたもの（売上ファイルrcdFiles）についてデータ読込、リストitemsに格納
		for(int i = 0; i < rcdFiles.size(); i++) {
			String rcdFilename = rcdFiles.get(i).getName();
			BufferedReader br = null;

			try{
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				List<String> items = new ArrayList<String>();

				while((line = br.readLine()) != null) {
					items.add(line);
				}

				// 3(2-4,2-5)売上ファイルのフォーマット（3行であるか）をチェック（×エラー表示、処理終了）
				if(items.size() != 3) {
					System.out.println(rcdFilename + FILENAME_FORMAT_ERROR);
					return;
				}

				// 3(2-3)売上ファイルの支店コードが支店定義ファイルに存在するかチェック（×エラー表示、処理終了）
				if(!branchNames.containsKey(items.get(0))) {
					System.out.println(rcdFilename + BRANCH_CODE_NOT_EXIST);
					return;
				}

				// 3(2-4)売上ファイルの商品コードが商品定義ファイルに存在するかチェック（×エラー表示、処理終了）
				if(!commodityNames.containsKey(items.get(1))) {
					System.out.println(rcdFilename + COMMODITY_CODE_NOT_EXIST);
					return;
				}

				// 3(3-2)売上ファイルの売上金額が数字かチェック（×エラー表示、処理終了）
				if(!items.get(2).matches(REGEX_NUMBER)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				// ⑤売上金額の型変換
				long fileSale = Long.parseLong(items.get(2));

				// ⑥-1売上金額をbranchSalesの支店コード（key）に対応するvalueに加算
				Long saleAmount = branchSales.get(items.get(0)) + fileSale;

				// ⑥-2売上金額をcommoditySalesの商品コード（key）に対応するvalueに加算
				Long commoditySaleAmount = commoditySales.get(items.get(1)) + fileSale;

				// 3(2-2)売上金額の合計が10桁を超えないかチェック（×エラー表示、処理終了）
				if((saleAmount >= 10000000000L) || (commoditySaleAmount >= 10000000000L)) {
					System.out.println(OVER_TEN_DIGITS);
					return;
				}
				branchSales.put(items.get(0), saleAmount);
				commoditySales.put(items.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 3-2商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名称を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @param ファイル名の一部（ファイルの種類）
	 * @param 正規表現
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales, String kind, String regex) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			// 3(1-1,1-3)ファイルの存在チェック（×エラー表示、処理終了）
			if(!file.exists()) {
				System.out.println(kind + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {

				String[] items = line.split(",");

				// 3(1-2,1-4)ファイルのフォーマットチェック（×エラー表示、処理終了）
				if((items.length != 2) || (!items[0].matches(regex))) {
					System.out.println(kind + FILE_INVALID_FORMAT);
					return false;
				}
				Names.put(items[0], items[1]);
				Sales.put(items[0], (long)0);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名称を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> Names, Map<String, Long> Sales) {
		BufferedWriter bw = null;

		// ①ファイルの作成、Mapからkeyを取得して、データを書き込み
		try {
			File file = new File(path,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : Names.keySet()) {
				bw.write(key + "," + Names.get(key) + "," + Sales.get(key).toString());
				bw.newLine();
			}

		} catch(IOException e){
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(bw != null) {
				try {
					// ファイルを閉じる
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}
}
